window.addEventListener('load', init);

var userDiv = document.querySelector('.user-div');
var computerDiv = document.querySelector('.computer-div');
var colorsDiv = document.querySelector('.by-color');
var playButton = document.querySelector('.play');
var scoreTable = document.querySelector('.score-table');
var userChoice = [];
var computerChoice = [];
var amount;
var iterator = 0;
var loop;
var myArr = [];
var computerTurnClone = myArr.concat(allNumbers);


function init() {

  let text = ``;

  for (let i = 0; i < allNumbers.length; i++) {

    text += `<div class="user-number"><p class="${allNumbers[i].color}">${allNumbers[i].number}</p></div>`;

  }

  userDiv.innerHTML = text;
  userDiv.addEventListener('click', selectNumber);

  colorsDiv.addEventListener('click', selectByColor);

  playButton.addEventListener('click', playGame);

}

function selectNumber(e) {

  if (e.target.tagName === 'P') {

    let selectedElement = e.target;
    let selectedNumber = parseInt(e.target.innerHTML);

    if (!isSelected(e) && userChoice.length < 6) {

      selectedElement.classList.add('selected');
      userChoice.push(selectedNumber);

    } else {

      selectedElement.classList.remove('selected');

      for (let i = 0; i < userChoice.length; i++) {

        if (selectedNumber === userChoice[i]) {

          userChoice.splice(i, 1);
          break;

        }

      }

    }

  }

}

function isSelected(e) {
  return e.target.classList.contains('selected');
}

function selectByColor(e) {

  if (e.target.tagName === 'SPAN') {

    userChoice = [];
    let choosenColor = e.target.getAttribute('class');
    let playerViewNumbers = document.querySelectorAll('.user-number>p');

    for (let i = 0; i < playerViewNumbers.length; i++) {

      playerViewNumbers[i].classList.remove('selected');

    }

    for (let i = 0; i < allNumbers.length; i++) {

      if (choosenColor === allNumbers[i].color) {

        userChoice.push(allNumbers[i].number);

        for (let j = 0; j < playerViewNumbers.length; j++) {

          let number = parseInt(playerViewNumbers[j].innerHTML);

          if (number === allNumbers[i].number) {

            playerViewNumbers[j].classList.add('selected');

          }

        }

      }

    }

  }

}

function playGame() {

  amount = document.querySelector('.bet-amount').value;

  if (userChoice.length < 6 || amount.length === 0) {

    alert(`Please check that you have chosen 6 numbers and entered the bet amount.\n Try again.`);

  } else {

    computersTurn();

  }

}

function computersTurn() {

  loop = setInterval(computerChooseNumber, 1000);

  userDiv.removeEventListener('click', selectNumber);
  colorsDiv.removeEventListener('click', selectByColor);
  playButton.removeEventListener('click', playGame);

}

function computerChooseNumber() {

  let playerViewNumbers = document.querySelectorAll('.user-number>p');

  if (iterator < 35) {

    let random = Math.floor(Math.random() * computerTurnClone.length);
    let newEl = document.createElement('div');
    newEl.setAttribute('class', `computer-number`);
    computerDiv.appendChild(newEl);

    if (iterator < 5) {

      computerChoice.push(computerTurnClone[random]);
      text = `<p class="${computerTurnClone[random].color}">${computerTurnClone[random].number}</p>`;


    } else {

      computerChoice.push(computerTurnClone[random]);
      computerChoice[iterator].coef = coefficient[iterator - 5];
      text = `<p class="${computerTurnClone[random].color}">${computerTurnClone[random].number}</p><span>${coefficient[iterator- 5]}</span>`;

    }

    for (let i = 0; i < userChoice.length; i++) {

      if (computerChoice[iterator].number === userChoice[i]) {


        for (let j = 0; j < playerViewNumbers.length; j++) {

          let x = parseInt(playerViewNumbers[j].innerHTML);

          if (x === userChoice[i]) {

            playerViewNumbers[j].classList.add('faded');

          }


        }

      }

    }

    newEl.innerHTML = text;
    computerTurnClone.splice(random, 1);

    iterator++;

  } else {

    compareChoices();
    clearInterval(loop);

  }

}

function compareChoices() {

  let matches = 0;
  let winner = 0;
  let coef = 0;

  for (let i = 0; i < computerChoice.length; i++) {

    for (let j = 0; j < userChoice.length; j++) {

      if (computerChoice[i].number === userChoice[j]) {

        matches++;

        if (matches === 6) {

          coef = computerChoice[i].coef;
          winner = amount * coef;
          declareWin(winner, coef);
          break;

        } else {

          declareLost();

        }

      }

    }

  }

}

function declareWin(money, coef) {

  let text =
    `<p>
  Fantastic! Your bet was ${amount}€. The coefficient of the last number was ${coef}. You won ${money}€</p><br><input type="button" value="Play again" class="replay form-control btn btn-success">`;
  scoreTable.innerHTML = text;

  enableReplay();

}

function declareLost() {

  let text =
    `<p>Sorry to see you loose ${amount}€. Click the button to try again.</p><br><input type="button" value="Play again" class="replay form-control btn btn-success">`;
  scoreTable.innerHTML = text;

  enableReplay();

}

function enableReplay() {

  let replayBtn = document.querySelector('.replay');
  replayBtn.addEventListener('click', replay);

}

function replay() {

  location.reload();

}