const coefficient = [
  10000, 7500, 5000, 2500, 1000, 500, 300, 200, 150, 100, 90, 80, 70, 60, 50, 40, 30, 25, 20, 15, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1
];

var allNumbers = [{
    number: 1,
    color: "red"
  },
  {
    number: 2,
    color: "green"
  },
  {
    number: 3,
    color: "blue"
  },
  {
    number: 4,
    color: "purple"
  },
  {
    number: 5,
    color: "brown"
  },
  {
    number: 6,
    color: "yellow"
  },
  {
    number: 7,
    color: "orange"
  },
  {
    number: 8,
    color: "white"
  },
  {
    number: 9,
    color: "red"
  }, {
    number: 10,
    color: "green"
  }, {
    number: 11,
    color: "blue"
  }, {
    number: 12,
    color: "purple"
  }, {
    number: 13,
    color: "brown"
  }, {
    number: 14,
    color: "yellow"
  }, {
    number: 15,
    color: "orange"
  }, {
    number: 16,
    color: "white"
  }, {
    number: 17,
    color: "red"
  }, {
    number: 18,
    color: "green"
  }, {
    number: 19,
    color: "blue"
  }, {
    number: 20,
    color: "purple"
  }, {
    number: 21,
    color: "brown"
  }, {
    number: 22,
    color: "yellow"
  }, {
    number: 23,
    color: "orange"
  }, {
    number: 24,
    color: "white"
  }, {
    number: 25,
    color: "red"
  }, {
    number: 26,
    color: "green"
  }, {
    number: 27,
    color: "blue"
  }, {
    number: 28,
    color: "purple"
  }, {
    number: 29,
    color: "brown"
  }, {
    number: 30,
    color: "yellow"
  }, {
    number: 31,
    color: "orange"
  }, {
    number: 32,
    color: "white"
  }, {
    number: 33,
    color: "red"
  }, {
    number: 34,
    color: "green"
  }, {
    number: 35,
    color: "blue"
  }, {
    number: 36,
    color: "purple"
  }, {
    number: 37,
    color: "brown"
  }, {
    number: 38,
    color: "yellow"
  }, {
    number: 39,
    color: "orange"
  }, {
    number: 40,
    color: "white"
  }, {
    number: 41,
    color: "red"
  }, {
    number: 42,
    color: "green"
  }, {
    number: 43,
    color: "blue"
  }, {
    number: 44,
    color: "purple"
  }, {
    number: 45,
    color: "brown"
  }, {
    number: 46,
    color: "yellow"
  }, {
    number: 47,
    color: "orange"
  }, {
    number: 48,
    color: "white"
  }
];